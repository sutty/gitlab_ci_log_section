require "time"
require "option_parser"

VERSION = "0.1.0"

class GitlabCiLogSection
  CL = "\e[0K"
  CR = "\r"

  getter name : String
  getter header : String

  def initialize(@name, @header) ; end

  def section_start
    String.build do |str|
      str << CL
      str << "section_start"
      str << ":"
      str << current_time
      str << ":"
      str << name
      str << CR
      str << CL
      str << header
    end
  end

  def section_end
    String.build do |str|
      str << CL
      str << "section_end"
      str << ":"
      str << current_time
      str << ":"
      str << name
      str << CR
      str << CL
    end
  end

  def current_time
    Time.utc.to_unix
  end
end

name = "gitlab_ci_log_section"
header = "Collapsible section"
mode = :start

OptionParser.parse do |p|
  p.banner = "Create Gitlab CI sections"

  p.on "-v", "--version", "Show version" do
    puts VERSION
    exit
  end

  p.on "-h", "--help", "Show help" do
    puts p
    exit
  end

  p.on "-n NAME", "--name=NAME", "Section name" do |n|
    name = n
  end

  p.on "-e", "--end", "End section" do
    mode = :end
  end

  p.on "-H HEADER", "--header=HEADER", "Header" do |h|
    header = h
  end
end

ci_log = GitlabCiLogSection.new(name, header)

case mode
when :end then puts ci_log.section_end
else puts ci_log.section_start
end
