gitlab_ci_log_section: src/gitlab_ci_log_section.cr
	crystal build --release $<
	strip --strip-all $@

install: gitlab_ci_log_section
	install -Dm755 $< $(PREFIX)/usr/bin/$<
