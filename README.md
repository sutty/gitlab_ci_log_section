# gitlab_ci_log_section

Prints [Gitlab CI
sections](https://docs.gitlab.com/ee/ci/jobs/#expand-and-collapse-job-log-sections)

## Installation

Install [Crystal](https://crystal-lang.org/) only for compiling.

```sh
# Archlinux dependencies
sudo pacman -S base-devel crystal pcre libevent
# Compile
make gitlab_ci_log_section
# Install
sudo make install
```

## Binary packages

### Gitlab Releases

[Download a release](https://0xacab.org/sutty/gitlab_ci_log_section/-/releases).  You
may need to install the package for `libevent` in your distribution.

### Alpine

Add [Sutty repositories](https://alpine.sutty.nl/):

Alpine 3.14: <https://alpine.sutty.nl/alpine/v3.14/sutty>

Alpine 3.17: <https://alpine.sutty.nl/alpine/v3.17/sutty>


## Usage

```yaml
# .gitlab-ci.yml
job:
  script:
  - "gitlab_ci_log_section --name job --header \"Installing dependencies\""
  - "bundle install"
  - "gitlab_ci_log_section --name job --end"
```

## Contributing

1. Fork it (<https://0xacab.org/sutty/gitlab_ci_log_section>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [fauno](https://0xacab.org/fauno)
